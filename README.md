# NFDI Metadata TF Contributions

NFDI contributions to the METADATA TaskForce.

* Re3Data dump from [API](https://www.re3data.org/api/) describe at the [documentation page](https://www.re3data.org/api/doc).

Relocation to [GitHub](https://github.com/ACz-UniBi/NFDI-Metadata-TF-Contributions) : https://github.com/ACz-UniBi/NFDI-Metadata-TF-Contributions  
